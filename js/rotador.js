jQuery(document).ready(function($) {
    $hook = $('.adslider');
    $hook.flexslider({
        animation: $hook.data('animation'),
        controlNav: $hook.data('control-nav'),
        slideshow: $hook.data('slideshow'),
        startAt: $hook.data('start-at'),
        pauseOnHover: true,
        controlNav: false,
        //al arrancar, establecemos la velocidad
        start: function(slider) {
    		$($hook).show(); 
            //la velocidad inicial
            var inicial = $(slider.slides[slider.currentSlide]).data('duration');
            //la establecemos
            clearInterval(slider.animatedSlides);
            slider.animatedSlides = setInterval(slider.animateSlides, inicial);
        },
        after: function(slider) {
            //tiempo de cada slider
            var tiempo = $(slider.slides[slider.currentSlide]).data('duration');
            // clears the interval
            slider.stop();
            // grab the duration to show this slide
            slider.vars.slideshowSpeed = tiempo;
            // start the interval
            slider.play();
        }
    });
});
