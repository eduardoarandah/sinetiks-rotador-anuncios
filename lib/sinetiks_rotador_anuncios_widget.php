<?php

class sinetiks_rotador_anuncios_widget extends WP_Widget
{

    public function __construct()
    {
        $id         = 'sinetiks-rotador-anuncios'; //css, no usar guion bajo
        $widget_ops = array(
            'classname'   => $id,
            'description' => '',
        );
        parent::__construct($id, '&raquo; Rotador de anuncios', $widget_ops);
    }

    public function widget($args, $instance)
    {

        //registrar script
        wp_enqueue_script('rotador', plugins_url().'/sinetiks-rotador-anuncios/js/rotador.js');
        $slot_id = $instance["slot_id"];
        $bloques = get_theme_mod('sinetiks_rotador_anuncios');

        if (isset($bloques) && count($bloques) > 0) {

            //localizamos los anuncios
            $anuncios = [];
            foreach ($bloques as $bloque) {
                if ($bloque['nombre'] == $slot_id) {
                    $anuncios = $bloque['anuncios'];
            		$altura=$bloque['altura'];
                }

            }
            if(count($anuncios)){            	
	            echo $args['before_widget'];
	            include plugin_dir_path(__FILE__) . '../views/widget.php';
	            echo $args['after_widget'];
            }
        }

    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        if (current_user_can('unfiltered_html')) {
            $instance['slot_id'] = $new_instance['slot_id'];
        } else {
            $instance['slot_id'] = wp_kses_post(stripslashes($new_instance['slot_id']));
        }

        return $instance;
    }

    public function form($instance)
    {
        $instance = wp_parse_args((array) $instance, []);
        $bloques  = get_theme_mod('sinetiks_rotador_anuncios');
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('slot_id'); ?>">Bloque de anuncios</label>
			<select
			class="widefat"
			id="<?php echo $this->get_field_id('slot_id'); ?>"
			name="<?php echo $this->get_field_name('slot_id'); ?>"
			>
				<?php foreach ($bloques as $bloque): ?>
					<option
					<?php if ($bloque['nombre'] == esc_attr($instance['slot_id'])) {
            echo "selected";
        }
        ?>
					>

					<?=$bloque['nombre']?>

					</option>
				<?php endforeach?>
			</select>

		</p>
		<?php
}
}
add_action('widgets_init', create_function('', 'return register_widget("sinetiks_rotador_anuncios_widget");'));
