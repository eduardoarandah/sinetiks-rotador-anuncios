<?php
// página
function sinetiks_rotador_anuncios_admin_menu_manage()
{
    include plugin_dir_path(__FILE__) . '../views/sinetiks_rotador_anuncios_admin_menu_manage.php';
}

//obtener valores en json
add_action('wp_ajax_sinetiks_rotador_anuncios_admin_menu_manage_ajax_get', 'sinetiks_rotador_anuncios_admin_menu_manage_ajax_get');
function sinetiks_rotador_anuncios_admin_menu_manage_ajax_get()
{
    echo json_encode(get_theme_mod('sinetiks_rotador_anuncios'));
    wp_die();
}

//guardar
add_action('wp_ajax_sinetiks_rotador_anuncios_admin_menu_manage_save', 'sinetiks_rotador_anuncios_admin_menu_manage_ajax_save');
function sinetiks_rotador_anuncios_admin_menu_manage_ajax_save()
{
    $data=$_POST['data'];    
    set_theme_mod('sinetiks_rotador_anuncios', $data);
    echo json_encode($data);
    wp_die();
}
