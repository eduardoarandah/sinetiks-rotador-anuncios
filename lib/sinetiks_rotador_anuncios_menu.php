<?php

function sinetiks_rotador_anuncios_menu()
{
    //this is a submenu
    add_menu_page(
        'Anuncios', //page title
        'Anuncios', //menu title
        'manage_options', //capability
        'sinetiks_rotador_anuncios_admin_menu_manage', //menu slug
        'sinetiks_rotador_anuncios_admin_menu_manage'); //function

}
add_action('admin_menu', 'sinetiks_rotador_anuncios_menu');
