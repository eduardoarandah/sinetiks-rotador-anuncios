<script src="https://unpkg.com/vue"> </script>
<style type="text/css">
    .sinetiks-anuncios th,.sinetiks-anuncios td{
        padding: 20px;
    }
</style>
<div class="wrap">

<h1>Bloques de anuncios</h1>

<div id="app">
    <div v-for="anuncio in anuncios">
        <table class="form-table widefat striped sinetiks-anuncios">
            <tr>
            <th>ID</th>
            <td><input type="text" name="id" class="" v-model="anuncio.id"/></td>
            </tr>
            <tr>
            <th>Nombre</th>
            <td><input type="text" name="name" class="" v-model="anuncio.name"/></td>
            </tr>
            <tr>
            <th>Descripcion</th>
            <td><input type="text" name="description" class="" v-model="anuncio.description"/></td>
            </tr>
            <tr><td colspan="2"><button v-on:click="borrar(anuncio)" class="button">Borrar</button></td></tr>
        </table>
    </div>
    <p>
        <button v-on:click="agregar" class="button">Agregar Uno</button>
        <button v-on:click="guardar" class="button button-primary">Guardar</button>
    </p>
    <!-- {{$data}} -->
</div>
</div>
<script type="text/javascript">
    var app = new Vue({
  el: '#app',
  data: {
    anuncios:[]
  },
  created:function(){
    jQuery(document).ready(function($) {
            var data = {
                'action': 'sinetiks_dynamic_anuncios_admin_menu_manage_ajax_get'
            };
            $.ajax({
                method:"POST",
                url:ajaxurl,
                data:data,
                success:function(response) {                
                    app.anuncios=jQuery.parseJSON(response);
                }
            });            
        });
  },
  methods:{
    agregar:function(){
        if(this.anuncios){
            this.anuncios.push({id:'',name:'',description:''});            
        }
        else{
            this.anuncios=[{id:'',name:'',description:''}];
        }
    },
    guardar:function(){
        var data = {
            'action': 'sinetiks_dynamic_anuncios_admin_menu_manage_save',
            'anuncios': this.anuncios
        };
        jQuery.post(ajaxurl, data, function(response) {            
            app.anuncios=jQuery.parseJSON(response);
            alert('Guardado Correctamente');
        });
    },
    borrar:function (anuncio) {
        this.anuncios.splice(anuncio,1);        
    }
  }
})
</script>