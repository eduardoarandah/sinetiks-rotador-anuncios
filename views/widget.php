<div class="adslider" data-animation="fade" data-control-nav="false" data-slideshow="true" data-start-at="0" style="height:<?=$altura?>px;display:none">
    <ul class="slides align-center">
<?php foreach ($anuncios as $anuncio): ?>
        <li data-duration="<?=$anuncio["tiempo"]?>">
            <?php if ($anuncio['url']): ?>
                <a href="<?=$anuncio['url']?>">
                    <img src="<?=$anuncio['img']?>" class="img img-responsive center-block" />                
                </a>
            <?php else: ?>
                <img src="<?=$anuncio['img']?>" class="img img-responsive center-block" />
            <?php endif ?>
        </li>        
<?php endforeach; ?>
    </ul>
</div>
