<!-- CDNJS :: Vue (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/vue/2.3.4/vue.min.js"></script>

<!-- CDNJS :: Sortable (https://cdnjs.com/) -->
<!-- https://github.com/RubaXa/Sortable -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Sortable/1.6.0/Sortable.min.js"></script>

<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
<!-- https://github.com/SortableJS/Vue.Draggable -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.14.1/vuedraggable.min.js"></script>

<style type="text/css">
    .tabla-anuncio{
        margin:0;
    }
    .tabla-anuncio th,.tabla-anuncio td{
        padding: 20px;
    }
    .td-img{
        width: 320px;
        text-align: center;
    }
    .td-img img{
        max-width: 320px;
        height:auto;
    }
    .bloque{
        border: 2px solid #ccc;
        padding: 0 20px 20px 20px;
        background-color: white;
        margin: 30px 0;
    }
    .anuncio {      
      transition: all 1s;
      margin-bottom: 15px;
      border: 2px solid white;
      position: relative; /*para posicionar el handle*/
    }
    .dashicons-dismiss{
        color:#a5a5a5;
    }
    /*manija para mover*/
    .handle{
        position: absolute;
        padding: 4px;
        right: 0;
        color:black;
    }
    /*cuando se toca*/
    .sortable-chosen{
        border: 2px solid gray;
    }
    /*fantasma cuando se mueve*/
    .sortable-ghost{
        border: 2px solid #ff8686; 
    }
    /*el contenido se aclara un poco*/
    .sortable-ghost .tabla-anuncio{
        opacity: .4;               
    }


</style>
<div class="wrap">

<h1>Bloques de anuncios</h1>

<div id="app">

    <div v-if="mensaje" class="notice notice-success is-dismissible"> 
        <p><strong>{{mensaje}}</strong></p>
        <button type="button" class="notice-dismiss" v-on:click="mensaje=''"> 
            <span class="screen-reader-text">Dismiss this notice.</span>
        </button>
    </div>

    <div v-for="(bloque,index) in bloques" class="bloque">   

        <h2>{{bloque.nombre}}</h2> 
        <p>
            <strong>Altura en pixeles </strong>
            <input type="text"  v-model="bloque.altura" >
        </p>

        <draggable v-model="bloque.anuncios" :options="{handle:'.handle'}">            
            <div class="anuncio" v-for="(anuncio,index) in bloque.anuncios" :key="index">
                
                <span class="handle dashicons dashicons-move"></span>     

                <table class="form-table widefat striped tabla-anuncio">                
                    <tr>
                    <th>Imagen URL</th>
                    <td><input type="text" name="img" v-model="anuncio.img" class="widefat" /></td>
                    <td rowspan="4" class="td-img">
                        <img v-bind:src="anuncio.img">&nbsp;
                    </td>
                    </tr>

                    <tr>
                    <th>Tiempo</th>
                    <td><input type="text" name="url" v-model="anuncio.tiempo" placeholder="milisegundos" /></td>
                    </tr>

                    <tr>
                    <th>Url</th>
                    <td><input type="text" name="url" v-model="anuncio.url" class="widefat" /></td>
                    </tr>

                    <tr><td colspan="2"><button v-on:click="borrarAnuncio(bloque,index)" class="button">Borrar</button></td></tr>
                </table>
            </div>
        </draggable>
    
        <button v-on:click="agregarAnuncio(bloque)" class="button">Agregar anuncio</button>
        <button v-if="!bloque.anuncios" v-on:click="borrarBloque(index)" class="button">Borrar bloque</button>
    </div>
    <p>
        <input type="bloqueNombre" v-model="bloqueNombre">
        <button v-on:click="agregarBloque" class="button">Agregar Bloque</button>
        <button v-on:click="guardar" class="button button-primary">Guardar</button>
    </p>
    <!-- {{$data}} -->
</div>
</div>
<script type="text/javascript">

    var app = new Vue({
  el: '#app',
  data: {
    bloques:[],
    bloqueNombre:'',
    mensaje:''
  },
  created:function(){
    jQuery(document).ready(function($) {
        var data = {
            'action': 'sinetiks_rotador_anuncios_admin_menu_manage_ajax_get'
        };
        $.ajax({
            method:"POST",
            url:ajaxurl,
            data:data,
            success:function(response) {                
                app.bloques=jQuery.parseJSON(response);
            }
        });            
    });
  },
  methods:{
    agregarBloque:function(){
        this.mensaje='';
        if(this.bloques=='')
            this.bloques=[];

        if(this.bloqueNombre){
            this.bloques.push({nombre:this.bloqueNombre, altura:0,anuncios:[]});            
            this.bloqueNombre='';
        }       
        
    },
    agregarAnuncio:function(bloque){
        this.mensaje='';
        if(bloque.anuncios=='')
            bloque.anuncios=[];
        
        bloque.anuncios.push({img:'',url:''});

    },
    guardar:function(){
        this.mensaje='';        
        var data = {
            'action': 'sinetiks_rotador_anuncios_admin_menu_manage_save',
            'data': this.bloques
        };
        jQuery.post(ajaxurl, data, function(response) {            
            app.bloques=jQuery.parseJSON(response);
            app.mensaje='Guardado Correctamente';
        });
    },
    borrarBloque:function(index){
        this.mensaje='';
        this.bloques.splice(index,1);
    },
    borrarAnuncio:function (bloque,index) {
        this.mensaje='';        
        bloque.anuncios.splice(index,1);        
    }
  }
})
</script>