<?php
/*
Plugin Name: Sinetiks rotador de anuncios
Description: Va cambiando anuncios en un widget
Version: 1.0
Author: Eduardo Aranda
Author URI: http://sinetiks.com/
*/

$includes = [
    'lib/sinetiks_rotador_anuncios_menu.php', //agregar a wp-admin como un menú
    'lib/sinetiks_rotador_anuncios_admin_menu_manage.php', //página para administrar 
    'lib/sinetiks_rotador_anuncios_widget.php', //widget
];
foreach ($includes as $file) {
    include plugin_dir_path(__FILE__) . $file;
}

